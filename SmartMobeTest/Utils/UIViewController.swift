//
//  UIViewController.swift
//  SmartMobeTest
//
//  Created by mac on 9/6/19.
//  Copyright © 2019 mac. All rights reserved.
//


import UIKit
import MBProgressHUD
extension UIViewController {
    func showHud(_ message: String) {
        let loadingNotification = MBProgressHUD.showAdded(to: (self.view)!, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        loadingNotification.bezelView.color = UIColor.clear
        loadingNotification.contentColor = UIColor.yellow
        loadingNotification.backgroundColor = UIColor.clear
        loadingNotification.isUserInteractionEnabled = false
    }
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
