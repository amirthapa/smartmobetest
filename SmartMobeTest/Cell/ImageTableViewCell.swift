//
//  ImageTableViewCell.swift
//  SmartMobeTest
//
//  Created by mac on 9/5/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var view_ImageCover: UIView!
    @IBOutlet weak var imv_Image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        addDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func addDesign(){
        view_ImageCover.layer.cornerRadius = 10
        view_ImageCover.layer.borderWidth = 2
        view_ImageCover.layer.borderColor = UIColor.yellow.cgColor
    }
    
}
