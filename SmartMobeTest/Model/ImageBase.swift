//
//  ImageBase.swift
//  SmartMobeTest
//
//  Created by mac on 9/6/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
struct ImageBase : Codable {
    let images : [Images]?
    
    enum CodingKeys: String, CodingKey {
        
        case images = "images"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        images = try values.decodeIfPresent([Images].self, forKey: .images)
    }
    
}
