//
//  Images.swift
//  SmartMobeTest
//
//  Created by mac on 9/6/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
struct Images : Codable {
    let id : Int?
    let url : String?
    let large_url : String?
    let source_id : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case url = "url"
        case large_url = "large_url"
        case source_id = "source_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        large_url = try values.decodeIfPresent(String.self, forKey: .large_url)
        source_id = try values.decodeIfPresent(Int.self, forKey: .source_id)
    }
    
}
