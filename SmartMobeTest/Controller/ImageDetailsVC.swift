//
//  ImageDetailsVC.swift
//  SmartMobeTest
//
//  Created by mac on 9/6/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Kingfisher

class ImageDetailsVC: UIViewController {
   
    @IBOutlet weak var l_Problem: UILabel!
    var imageUrl = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        scrollView.delegate = self
    
        loadImage()
       
      
    }



    @IBAction func clickCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    private func loadImage(){
        let imageUrl = URL(string: self.imageUrl)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: imageUrl, options: [.memoryCacheExpiration(.expired)]) { result in
            self.scrollView.minimumZoomScale = 1
            self.scrollView.maximumZoomScale = 5
            DispatchQueue.main.async {
                self.scrollView.zoomScale = 0
            }
            switch result {
            case .success(_):
                self.l_Problem.isHidden = true
            case .failure(_):
                self.l_Problem.isHidden = false
                
            }
            
        }
        
        
    }
    

}
extension ImageDetailsVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
