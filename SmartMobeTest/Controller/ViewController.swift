//
//  ViewController.swift
//  SmartMobeTest
//
//  Created by mac on 9/5/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import DZNEmptyDataSet


class ViewController: UIViewController {
    @IBOutlet weak var l_NoData: UILabel!
    
      @IBOutlet var tb_Images: UITableView!
    let noInternet = "Please check you internet and Refresh again"
    let noData = "No data available"
    let imageUrl = "http://www.splashbase.co/api/v1/images/latest?images_only=true"
    
    let cellIdentifier = "ImageTableViewCell"
    let searchController = UISearchController(searchResultsController: nil)
    var images = [Images]()
    var filteredImages = [Images]()
    var filterText = ""
 

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Images"
         self.l_NoData.isHidden = true
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white
            
        }
        searchController.searchBar.delegate = self
        
        //setup tb_Images
        tb_Images.dataSource = self
        tb_Images.delegate = self
        tb_Images.register(UINib(nibName: "ImageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        
        //refresh Button
        let refresh = UIBarButtonItem(image: UIImage(named: "refresh"), style: .plain, target: self, action: #selector(self.refresh))
        self.navigationItem.rightBarButtonItem  = refresh
        loadImages(query: "")
     
    }
    @objc func refresh(){
        if isFiltering(){
           loadImages(query: filterText)
        }else{
            loadImages(query: "")
        }
    }
    
    func loadImages(query:String!){
        
            showHud("Loading...")
  
        var url = ""
        if query == ""{
          url = imageUrl
        }else{
           url = "http://www.splashbase.co/api/v1/images/search?query="+query
        }
      
        print(url)
        Alamofire.request(url).responseJSON { response in
            switch response.result {
            case .success:
                print(response)
                let json = JSON(response.data as Any)
                if json["images"].arrayValue.count != 0{
                    self.l_NoData.isHidden = true
                    do {
                        //here dataResponse received from a network request
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(ImageBase.self, from:
                            response.data!)
                        //Decode  Response Data
                        
                        if query == ""{
                            self.images.removeAll()
                            self.images = model.images!
                        }else{
                            self.filteredImages.removeAll()
                            self.filteredImages = model.images!
                        }
                      
                       self.tb_Images.reloadData()
                    } catch let parsingError {
                        print("Error", parsingError)
                    }
                }else{
                    self.l_NoData.isHidden = false
                    self.l_NoData.text = self.noData
                        self.filteredImages.removeAll()
                    self.tb_Images.reloadData()
                    }
                self.hideHUD()
                
            case .failure( _):
                if self.filteredImages.count == 0 ||  self.images.count == 0{
                     self.l_NoData.isHidden = false
                    self.l_NoData.text = self.noInternet
                }
             self.tb_Images.reloadData()
                self.hideHUD()
            }
            
          
        }
        
        
        
        
    }
    
    // MARK: - filter the image
    func filterContentForSearchText(_ searchText: String) {
        filterText = searchText
     loadImages(query: searchText)
    }
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
}

extension ViewController :UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredImages.count
        }
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ImageTableViewCell
        cell?.selectionStyle = .none
        let image:Images
        if isFiltering() {
            image = filteredImages[indexPath.row]
        } else {
            image = images[indexPath.row]
        }
     
            let imageUrl = URL(string: image.url!)
            cell?.imv_Image.kf.setImage(with: imageUrl, placeholder:#imageLiteral(resourceName: "placeholder"))
        
     
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let image:Images
        if isFiltering() {
            image = filteredImages[indexPath.row]
        } else {
            image = images[indexPath.row]
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let imageDetails = storyboard.instantiateViewController(withIdentifier: "ImageDetailsVC") as! ImageDetailsVC
        imageDetails.imageUrl = image.large_url!
        self.navigationController?.present(imageDetails, animated: true, completion: nil)
        
        
    }
    
    
    
    
}
    
extension ViewController: UISearchBarDelegate {
        // MARK: - UISearchBar Delegate
        func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
            filterContentForSearchText(searchBar.text!)
        }
    }
    
    extension ViewController: UISearchResultsUpdating {
        // MARK: - UISearchResultsUpdating Delegate
        func updateSearchResults(for searchController: UISearchController) {
            
            filterContentForSearchText(searchController.searchBar.text!)
        }
    }


    
    





